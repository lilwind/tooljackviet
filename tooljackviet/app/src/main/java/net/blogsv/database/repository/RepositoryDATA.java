package net.blogsv.database.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import net.blogsv.database.Database;
import net.blogsv.database.dao.DataDAO;
import net.blogsv.database.entity.AnhEntity;
import net.blogsv.database.entity.AnhMoiEntity;
import net.blogsv.database.entity.CongNgheEntity;
import net.blogsv.database.entity.DiaEntity;
import net.blogsv.database.entity.GDCDEntity;
import net.blogsv.database.entity.HoaEntity;
import net.blogsv.database.entity.HoaNCEntity;
import net.blogsv.database.entity.LyEntity;
import net.blogsv.database.entity.LyNCEntity;
import net.blogsv.database.entity.SinhEntity;
import net.blogsv.database.entity.SinhNCEntity;
import net.blogsv.database.entity.SoanVanEntity;
import net.blogsv.database.entity.SuEntity;
import net.blogsv.database.entity.TGTPEntity;
import net.blogsv.database.entity.TinEntity;
import net.blogsv.database.entity.ToanEntity;
import net.blogsv.database.entity.ToanNCEntity;
import net.blogsv.database.entity.VanMauEntity;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by LilWind NTS on 10/09/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public class RepositoryDATA {
    ExecutorService service;
    DataDAO dataDAO;


    public RepositoryDATA(Application application) {
        service = Executors.newSingleThreadExecutor();
        dataDAO = Database.getInstance(application).dataDAO();
    }

    public void insertGDCD(final GDCDEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataGDCD(data);
                }
            }
        });
    }

    public void insertAnh(final AnhEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataAnh(data);
                }
            }
        });
    }

    public void insertAnhMoi(final AnhMoiEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataAnhMoi(data);
                }
            }
        });
    }

    public void insertCongNghe(final CongNgheEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataCN(data);
                }
            }
        });
    }

    public void insertDia(final DiaEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataDia(data);
                }
            }
        });
    }

    public void insertHoa(final HoaEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataHoa(data);
                }
            }
        });
    }

    public void insertLy(final LyEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataLy(data);
                }
            }
        });
    }

    public void insertSinh(final SinhEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataSinh(data);
                }
            }
        });
    }

    public void insertSoanVan(final SoanVanEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataSoanVan(data);
                }
            }
        });
    }

    public void insertSu(final SuEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataSu(data);
                }
            }
        });
    }

    public void insertTin(final TinEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataTin(data);
                }
            }
        });
    }

    public void insertToan(final ToanEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataToan(data);
                }
            }
        });
    }

    public void insertVanMau(final VanMauEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataVanMau(data);
                }
            }
        });
    }

    public void insertTGTP(final TGTPEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataTGTP(data);
                }
            }
        });
    }

    public void insertHoaNC(final HoaNCEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataHoaNC(data);
                }
            }
        });
    }

    public void insertLyNC(final LyNCEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataLyNC(data);
                }
            }
        });
    }

    public void insertToanNC(final ToanNCEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataToanNC(data);
                }
            }
        });
    }

    public void insertSinhNC(final SinhNCEntity data) {
        service.execute(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    dataDAO.insertDataSinhNC(data);
                }
            }
        });
    }

    /// Respon  get Data
    public LiveData<List<AnhEntity>> getDataAnh() {
        return dataDAO.getDataAnh();
    }

    public LiveData<List<AnhMoiEntity>> getDataAnhMoi() {
        return dataDAO.getDataAnhMoi();
    }

    public LiveData<List<CongNgheEntity>> getDataCongNghe() {
        return dataDAO.getDataCongNghe();
    }

    public LiveData<List<DiaEntity>> getDataDia() {
        return dataDAO.getDataDia();
    }

    public LiveData<List<GDCDEntity>> getDataGDCD() {
        return dataDAO.getDataGDCD();
    }

    public LiveData<List<HoaEntity>> getDataHoa() {
        return dataDAO.getDataHoa();
    }

    public LiveData<List<LyEntity>> getDataLy() {
        return dataDAO.getDataLy();
    }

    public LiveData<List<SinhEntity>> getDataSinh() {
        return dataDAO.getDataSinh();
    }

    public LiveData<List<SoanVanEntity>> getDataSoanVan() {
        return dataDAO.getDataSoanVan();
    }

    public LiveData<List<SuEntity>> getDataSu() {
        return dataDAO.getDataSu();
    }

    public LiveData<List<TGTPEntity>> getDataTGTP() {
        return dataDAO.getDataTGTP();
    }

    public LiveData<List<TinEntity>> getDataTin() {
        return dataDAO.getDataTin();
    }

    public LiveData<List<ToanEntity>> getDataToan() {
        return dataDAO.getDataToan();
    }

    public LiveData<List<VanMauEntity>> getDataVanMau() {
        return dataDAO.getDataVanMau();
    }

    public LiveData<List<ToanNCEntity>> getDataToanNC() {
        return dataDAO.getDataToanNC();
    }

    public LiveData<List<HoaNCEntity>> getDataHoaNC() {
        return dataDAO.getDataHoaNC();
    }

    public LiveData<List<LyNCEntity>> getDataLyNC() {
        return dataDAO.getDataLyNC();
    }

    public LiveData<List<SinhNCEntity>> getDataSinhNC() {
        return dataDAO.getDataSinhNC();
    }
    public LiveData<String> getContentLyById(int id) {
        return dataDAO.getContentLyById(id);
    }
}
