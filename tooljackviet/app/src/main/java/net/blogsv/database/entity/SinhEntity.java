package net.blogsv.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by LilWind NTS on 10/09/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
@Entity
public class SinhEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo
    private String h2;
    @ColumnInfo
    private String h3;
    @ColumnInfo
    private String ul;
    @ColumnInfo
    private String ul2;
    @ColumnInfo
    private String contentUl;
    private int like = 0;

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getH2() {
        return h2;
    }

    public void setH2(String h2) {
        this.h2 = h2;
    }

    public String getH3() {
        return h3;
    }

    public void setH3(String h3) {
        this.h3 = h3;
    }

    public String getUl() {
        return ul;
    }

    public void setUl(String ul) {
        this.ul = ul;
    }

    public String getUl2() {
        return ul2;
    }

    public void setUl2(String ul2) {
        this.ul2 = ul2;
    }

    public String getContentUl() {
        return contentUl;
    }

    public void setContentUl(String contentUl) {
        this.contentUl = contentUl;
    }
}

