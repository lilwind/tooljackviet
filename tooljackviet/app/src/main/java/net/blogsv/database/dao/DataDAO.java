package net.blogsv.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import net.blogsv.database.entity.AnhEntity;
import net.blogsv.database.entity.AnhMoiEntity;
import net.blogsv.database.entity.CongNgheEntity;
import net.blogsv.database.entity.DiaEntity;
import net.blogsv.database.entity.GDCDEntity;
import net.blogsv.database.entity.HoaEntity;
import net.blogsv.database.entity.HoaNCEntity;
import net.blogsv.database.entity.LyEntity;
import net.blogsv.database.entity.LyNCEntity;
import net.blogsv.database.entity.SinhEntity;
import net.blogsv.database.entity.SinhNCEntity;
import net.blogsv.database.entity.SoanVanEntity;
import net.blogsv.database.entity.SuEntity;
import net.blogsv.database.entity.TGTPEntity;
import net.blogsv.database.entity.TinEntity;
import net.blogsv.database.entity.ToanEntity;
import net.blogsv.database.entity.ToanNCEntity;
import net.blogsv.database.entity.VanMauEntity;

import java.util.List;

;

/**
 * Created by LilWind NTS on 10/09/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
@Dao
public interface DataDAO {
    @Insert()
    void insertDataAnh(AnhEntity data);

    @Insert()
    void insertDataAnhMoi(AnhMoiEntity data);

    @Insert()
    void insertDataCN(CongNgheEntity data);

    @Insert()
    void insertDataDia(DiaEntity data);

    @Insert()
    void insertDataGDCD(GDCDEntity data);

    @Insert()
    void insertDataHoa(HoaEntity data);

    @Insert()
    void insertDataLy(LyEntity data);

    @Insert()
    void insertDataSinh(SinhEntity data);

    @Insert()
    void insertDataSoanVan(SoanVanEntity data);

    @Insert()
    void insertDataSu(SuEntity data);

    @Insert()
    void insertDataTin(TinEntity data);

    @Insert()
    void insertDataToan(ToanEntity data);

    @Insert()
    void insertDataVanMau(VanMauEntity data);

    @Insert()
    void insertDataTGTP(TGTPEntity data);
    
    @Insert()
    void insertDataHoaNC(HoaNCEntity data);

    @Insert()
    void insertDataToanNC(ToanNCEntity data);
    
    @Insert()
    void insertDataLyNC(LyNCEntity data);
    
    @Insert()
    void insertDataSinhNC(SinhNCEntity data);

    ///Get data


    @Query("SELECT * FROM AnhEntity")
    LiveData<List<AnhEntity>> getDataAnh();

    @Query("SELECT * FROM AnhMoiEntity")
    LiveData<List<AnhMoiEntity>> getDataAnhMoi();

    @Query("SELECT * FROM CongNgheEntity")
    LiveData<List<CongNgheEntity>> getDataCongNghe();

    @Query("SELECT * FROM DiaEntity")
    LiveData<List<DiaEntity>> getDataDia();

    @Query("SELECT * FROM GDCDEntity")
    LiveData<List<GDCDEntity>> getDataGDCD();

    @Query("SELECT * FROM HoaEntity")
    LiveData<List<HoaEntity>> getDataHoa();

    @Query("SELECT * FROM LyEntity")
    LiveData<List<LyEntity>> getDataLy();

    @Query("SELECT * FROM SinhEntity")
    LiveData<List<SinhEntity>> getDataSinh();

    @Query("SELECT * FROM SoanVanEntity")
    LiveData<List<SoanVanEntity>> getDataSoanVan();

    @Query("SELECT * FROM SuEntity")
    LiveData<List<SuEntity>> getDataSu();

    @Query("SELECT * FROM TGTPEntity")
    LiveData<List<TGTPEntity>> getDataTGTP();

    @Query("SELECT * FROM TinEntity")
    LiveData<List<TinEntity>> getDataTin();

    @Query("SELECT * FROM ToanEntity")
    LiveData<List<ToanEntity>> getDataToan();

    @Query("SELECT * FROM VanMauEntity")
    LiveData<List<VanMauEntity>> getDataVanMau();

    @Query("SELECT * FROM ToanNCEntity")
    LiveData<List<ToanNCEntity>> getDataToanNC();

    @Query("SELECT * FROM HoaNCEntity")
    LiveData<List<HoaNCEntity>> getDataHoaNC();

    @Query("SELECT * FROM LyNCEntity")
    LiveData<List<LyNCEntity>> getDataLyNC();

    @Query("SELECT * FROM SinhNCEntity")
    LiveData<List<SinhNCEntity>> getDataSinhNC();


    @Query("SELECT LyEntity.contentUl FROM LyEntity WHERE LyEntity.id =:id")
    LiveData<String> getContentLyById(int id);
}
