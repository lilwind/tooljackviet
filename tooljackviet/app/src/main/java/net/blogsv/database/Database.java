package net.blogsv.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import net.blogsv.database.dao.DataDAO;
import net.blogsv.database.entity.AnhEntity;
import net.blogsv.database.entity.AnhMoiEntity;
import net.blogsv.database.entity.CongNgheEntity;
import net.blogsv.database.entity.DiaEntity;
import net.blogsv.database.entity.GDCDEntity;
import net.blogsv.database.entity.HoaEntity;
import net.blogsv.database.entity.HoaNCEntity;
import net.blogsv.database.entity.LyEntity;
import net.blogsv.database.entity.LyNCEntity;
import net.blogsv.database.entity.SinhEntity;
import net.blogsv.database.entity.SinhNCEntity;
import net.blogsv.database.entity.SoanVanEntity;
import net.blogsv.database.entity.SuEntity;
import net.blogsv.database.entity.TGTPEntity;
import net.blogsv.database.entity.TinEntity;
import net.blogsv.database.entity.ToanEntity;
import net.blogsv.database.entity.ToanNCEntity;
import net.blogsv.database.entity.VanMauEntity;

/**
 * Created by LilWind NTS on 9/10/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
@androidx.room.Database(entities = {HoaNCEntity.class, LyNCEntity.class, SinhNCEntity.class, ToanNCEntity.class, AnhEntity.class, AnhMoiEntity.class, CongNgheEntity.class, DiaEntity.class, GDCDEntity.class, HoaEntity.class, LyEntity.class, SinhEntity.class, SoanVanEntity.class, SuEntity.class, TinEntity.class, ToanEntity.class, VanMauEntity.class, TGTPEntity.class}, version = 1)
public abstract class Database extends RoomDatabase {
    private static volatile Database INSTANCE = null;

    public static Database getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (Database.class) {
                try {
                    INSTANCE = Room.databaseBuilder(context, Database.class, "sgk12.sqlite").build();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return INSTANCE;
        }
        return INSTANCE;
    }

    public abstract DataDAO dataDAO();

}
