package net.blogsv.adater;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.blogsv.database.entity.LyEntity;
import net.blogsv.showdata.ly.ContentLyActivity;
import net.blogsv.tooljackviet.R;

import java.util.List;

/**
 * Created by LilWind NTS on 9/11/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public class LyAdapter extends RecyclerView.Adapter<LyAdapter.Viewholer> {
    private Context context;
    private List<LyEntity> ly11Entities;

    public LyAdapter(Context context, List<LyEntity> ly11Entities) {
        this.context = context;
        this.ly11Entities = ly11Entities;
    }

    @NonNull
    @Override
    public Viewholer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new Viewholer(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addItems(List<LyEntity> data) {
        ly11Entities = data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholer holder, int position) {
        final LyEntity lyEntity = ly11Entities.get(position);
        System.out.println("data adapter " + lyEntity.toString());

        if (lyEntity.getH2() == null) {
            holder.h2.setVisibility(View.GONE);
        } else {
            System.out.println("h2 " + lyEntity.getH2());
            holder.h2.setText(lyEntity.getH2());
        }

        if (lyEntity.getH3() == null) {
            holder.h3.setVisibility(View.GONE);
        } else {
            holder.h3.setText(lyEntity.getH3());
            System.out.println("h3 " + lyEntity.getH3());
        }

        if (lyEntity.getUl() == null) {
            holder.ul.setVisibility(View.GONE);
        } else {
            holder.ul.setText(lyEntity.getUl());
        }

        if (lyEntity.getUl2() == null) {
            holder.ul2.setVisibility(View.GONE);
        } else {
            holder.ul2.setText(lyEntity.getUl2());
            holder.ul2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ContentLyActivity.class);
                    intent.putExtra("id", lyEntity.getId());
                    intent.putExtra("datacontet", lyEntity.getContentUl());
                    context.startActivity(intent);
                }
            });
        }


//        //h2
//        if (lyEntity.getH2()!=null) {
//            holder.h2.setVisibility(View.VISIBLE);
//            holder.h2.setText(lyEntity.getH2());
//        }else {
//            holder.h2.setVisibility(View.GONE);
//        }
//        if (lyEntity.getH3()!=null) {
//            holder.h2.setVisibility(View.VISIBLE);
//            holder.h2.setText(lyEntity.getH3());
//
//        }else {
//            holder.h2.setVisibility(View.GONE);
//        }
//
//        //h3
//        if (lyEntity.getUl()!=null) {
//            holder.h3.setVisibility(View.VISIBLE);
//            holder.h3.setText(lyEntity.getUl());
//
//        }else {
//            holder.h3.setVisibility(View.GONE);
//        }
//        //ul
//        if (lyEntity.getUl2()!=null) {
//            holder.ul.setText(lyEntity.getUl2());
//        }else {
//            holder.h3.setVisibility(View.GONE);
//        }


    }


    @Override
    public int getItemCount() {
        if (ly11Entities == null) {
            return 0;
        } else {
            return ly11Entities.size();
        }
    }

    public class Viewholer extends RecyclerView.ViewHolder {
        TextView h2, h3, ul, ul2;

        public Viewholer(@NonNull View itemView) {
            super(itemView);
            h2 = itemView.findViewById(R.id.tv_h2);
            h3 = itemView.findViewById(R.id.tv_h3);
            ul = itemView.findViewById(R.id.tv_ul);
            ul2 = itemView.findViewById(R.id.tv_ul2);
        }
    }
}
