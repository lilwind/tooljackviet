package net.blogsv.adater;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import net.blogsv.database.entity.DiaEntity;
import net.blogsv.showdata.ly.ContentLyActivity;
import net.blogsv.tooljackviet.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by LilWind NTS on 9/11/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public class DiaAdapter extends RecyclerView.Adapter<DiaAdapter.Viewholer> {
    private Context context;
    private List<DiaEntity> datas;

    public DiaAdapter(Context context, List<DiaEntity> datas) {
        this.context = context;
        this.datas = datas;
    }

    @NonNull
    @Override
    public Viewholer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new Viewholer(view);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addItems(List<DiaEntity> data) {
        datas = data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholer holder, final int position) {
        final DiaEntity data = datas.get(position);
        System.out.println("data adapter " + data.toString());

        if (data.getH2() == null) {
            holder.h2.setVisibility(View.GONE);
        } else {
            System.out.println("h2 " + data.getH2());
            holder.h2.setText(data.getH2());
        }

        if (data.getH3() == null) {
            holder.h3.setVisibility(View.GONE);
        } else {
            holder.h3.setText(data.getH3());
            System.out.println("h3 " + data.getH3());
        }

        if (data.getUl() == null) {
            holder.ul.setVisibility(View.GONE);
        } else {
            holder.ul.setText(data.getUl());
        }

        SharedPreferences sharedPreferences = context.getSharedPreferences("DIA", MODE_PRIVATE);
        System.out.println("position" +sharedPreferences.getInt("position",0));
        if (sharedPreferences.getInt("position",0) == position&&sharedPreferences.getInt("position",0)!=0){
            holder.imgTick.setVisibility(View.VISIBLE);
            holder.ul2.setTextColor(ContextCompat.getColor(context, R.color.colorRed));
        }else {
            holder.imgTick.setVisibility(View.GONE);
        }

        if (data.getUl2() == null) {
            holder.ul2.setVisibility(View.GONE);
        } else {
            holder.ul2.setText(data.getUl2());
            holder.ul2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ContentLyActivity.class);
                    intent.putExtra("title", data.getUl2());
                    intent.putExtra("datacontet", data.getContentUl());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    luuPosition(position);
                    context.startActivity(intent);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        if (datas == null) {
            return 0;
        } else {
            return datas.size();
        }
    }

    public class Viewholer extends RecyclerView.ViewHolder {
        TextView h2, h3, ul, ul2;
        ImageView imgTick;

        public Viewholer(@NonNull View itemView) {
            super(itemView);
            h2 = itemView.findViewById(R.id.tv_h2);
            h3 = itemView.findViewById(R.id.tv_h3);
            ul = itemView.findViewById(R.id.tv_ul);
            ul2 = itemView.findViewById(R.id.tv_ul2);
            imgTick = itemView.findViewById(R.id.img_tick);
        }
    }

    private void luuPosition(int posion) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("DIA", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("position", posion);
        editor.apply();
    }


}
