package net.blogsv.retrofit2;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by LilWind NTS on 11/09/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public interface ApiService {
    @Streaming
    @GET
    Call<ResponseBody> downloadFileURL(@Url String fileUrl);
}
