package net.blogsv.retrofit2;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by LilWind NTS on 9/11/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public class DownloadFile {
    private static final String TAG = "gggg";
    private static Retrofit retrofit;
    private String url;
    private String nameFile;
    private String nameFolder;
    private Context context;
    private ExecutorService service;

    public DownloadFile(String url, String nameFile, String nameFolder, Context context) {
        this.url = url;
        this.context = context;
        this.nameFile = nameFile;
        this.nameFolder = nameFolder;
        service = Executors.newSingleThreadExecutor();
    }

    private ApiService initRetrofit() {
        if (retrofit == null) {
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://vietjack.com/giai-bai-tap-vat-ly-11/");
            retrofit = builder.build();
        }
        return retrofit.create(ApiService.class);
    }

    public void download() {
        //Create retrofi
        System.out.println("sub chay");
//        synchronized (this) {
        //toget Cline
        ApiService fileDownload = initRetrofit();
        Call<ResponseBody> call = fileDownload.downloadFileURL(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
//                writeResponseBodyToDisk(response.body());
                service.execute(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void run() {
                        try {
                            boolean stt = writeResponseBodyToDisk(response.body());
                            System.out.println(stt + " da ghi");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                            Toast.makeText(context, ss + "", Toast.LENGTH_SHORT).show();
                    }
                });


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
//        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private boolean writeResponseBodyToDisk(ResponseBody body) {

        synchronized (this) {
            try {
                // todo change the file location/name according to your needs
                File futureStudioIconFile = new File(context.getExternalFilesDir(nameFolder) + File.separator + nameFile);
//                File futureStudioIconFile = new File(context.getFilesDir().+nameFolder, nameFile);
                InputStream inputStream = null;
                OutputStream outputStream = null;

                try {
                    byte[] fileReader = new byte[4096];
                    long fileSize = 0;
                    long fileSizeDownloaded = 0;
                    outputStream = new FileOutputStream(futureStudioIconFile);
                    if (body != null) {
                        fileSize = body.contentLength();
                        inputStream = body.byteStream();
                        while (true) {
                            int read = inputStream.read(fileReader);

                            if (read == -1) {
                                break;
                            }

                            outputStream.write(fileReader, 0, read);

                            fileSizeDownloaded += read;

                            Log.d("download file", "file download: " + fileSizeDownloaded + " of " + fileSize);
                        }
                    }


                    outputStream.flush();

                    return true;
                } catch (IOException e) {
                    return false;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }

                    if (outputStream != null) {
                        outputStream.close();
                    }
                }
            } catch (IOException e) {
                return false;
            }
        }

    }

    //    private void saveToDisk(ResponseBody body, String filename) {
//        try {
//
//            File destinationFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);
//
//            InputStream inputStream = null;
//            OutputStream outputStream = null;
//
//            try {
//
//                inputStream = body.byteStream();
//                outputStream = new FileOutputStream(destinationFile);
//                byte data[] = new byte[4096];
//                int count;
//                int progress = 0;
//                long fileSize = body.contentLength();
//                Log.d(TAG, "File Size=" + fileSize);
//                while ((count = inputStream.read(data)) != -1) {
//                    outputStream.write(data, 0, count);
//                    progress += count;
//                    Pair<Integer, Long> pairs = new Pair<>(progress, fileSize);
//                    downloadZipFileTask.doProgress(pairs);
//                    Log.d(TAG, "Progress: " + progress + "/" + fileSize + " >>>> " + (float) progress / fileSize);
//                }
//
//                outputStream.flush();
//
//                Log.d(TAG, destinationFile.getParent());
//                Pair<Integer, Long> pairs = new Pair<>(100, 100L);
//                downloadZipFileTask.doProgress(pairs);
//                return;
//            } catch (IOException e) {
//                e.printStackTrace();
//                Pair<Integer, Long> pairs = new Pair<>(-1, Long.valueOf(-1));
//                downloadZipFileTask.doProgress(pairs);
//                Log.d(TAG, "Failed to save the file!");
//                return;
//            } finally {
//                if (inputStream != null) inputStream.close();
//                if (outputStream != null) outputStream.close();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.d(TAG, "Failed to save the file!");
//            return;
//        }
//    }
//
//    private void askForPermission(String permission, Integer requestCode) {
//        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
//
//
//            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
//                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
//
//            } else {
//                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
//            }
//        } else if (ContextCompat.checkSelfPermission(MainActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
//            Toast.makeText(getApplicationContext(), "Permission was denied", Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
//
//            if (requestCode == 101)
//                Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
//        }
//    }
//    private class DownloadZipFileTask extends AsyncTask<ResponseBody, Pair<Integer, Long>, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//        }
//
//        @Override
//        protected String doInBackground(ResponseBody... urls) {
//            //Copy you logic to calculate progress and call
//            saveToDisk(urls[0], "journaldev-project.zip");
//            return null;
//        }
//
//        protected void onProgressUpdate(Pair<Integer, Long>... progress) {
//
//            Log.d("API123", progress[0].second + " ");
//
//            if (progress[0].first == 100)
//                Toast.makeText(context.getApplicationContext(), "File downloaded successfully", Toast.LENGTH_SHORT).show();
//
//
//            if (progress[0].second > 0) {
//                int currentProgress = (int) ((double) progress[0].first / (double) progress[0].second * 100);
//                progressBar.setProgress(currentProgress);
//
//                txtProgressPercent.setText("Progress " + currentProgress + "%");
//
//            }
//
//            if (progress[0].first == -1) {
//                Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_SHORT).show();
//            }
//
//        }
//
//        public void doProgress(Pair<Integer, Long> progressDetails) {
//            publishProgress(progressDetails);
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//
//        }
//    }
}
