package net.blogsv.craw;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import net.blogsv.database.entity.CongNgheEntity;
import net.blogsv.database.repository.RepositoryDATA;
import net.blogsv.retrofit2.DownloadFile;
import net.blogsv.tooljackviet.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by LilWind NTS on 10/09/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public class CongNgheActivity extends AppCompatActivity {
    GifImageView gifImageView;
    WebView webView;
    Button btnCraw;
    ExecutorService service;
    ExecutorService threadPool;
    RepositoryDATA repositoryDATA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_craw);
        repositoryDATA = new RepositoryDATA(getApplication());
        service = Executors.newSingleThreadExecutor();
        threadPool = Executors.newFixedThreadPool(30);
        addControls();
        addEvents();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void addControls() {
        gifImageView = findViewById(R.id.img_cao);
        webView = findViewById(R.id.webview);
        btnCraw = findViewById(R.id.btnSubmit);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36");
        } else {
            webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36");
        }
        webView.getSettings().setJavaScriptEnabled(true);
    }

    private void addEvents() {
        btnCraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gifImageView.setVisibility(View.VISIBLE);
                service.execute(new Runnable() {
                    @Override
                    public void run() {
                        crawData();

                    }
                });
            }
        });

    }

    private void crawData() {
        try {
            Document document = Jsoup.connect("https://vietjack.com/giai-bai-tap-cong-nghe-12/index.jsp").get();
            Element body = document.getElementsByClass("col-md-7 middle-col").get(0);
            System.out.println("data  body " + body.text());
            body.getElementsByClass("ads_txt").remove();
            body.getElementsByClass("ads_ads").remove();
            body.getElementsByClass("vj-more").remove();
            body.getElementsByClass("baner-commer").remove();
            System.out.println("data c1" + body);
            for (Element e :
                    body.children()) {
                if (!e.tagName().equals("h2")) {
                    e.remove();
                } else {
                    break;
                }
            }
            for (final Element e : body.children()
            ) {
                if (e.tagName().equals("h3") && !e.attr("style").isEmpty()) {
                    break;
                }
                if (e.tagName().equals("h2")) {
                    CongNgheEntity data = new CongNgheEntity();
                    data.setH2(e.text());
                    repositoryDATA.insertCongNghe(data);
                } else if (e.tagName().equals("h3")) {
                    CongNgheEntity data = new CongNgheEntity();
                    data.setH3(e.text());
                    repositoryDATA.insertCongNghe(data);
                } else if (e.tagName().equals("ul")) {
                    for (final Element li : e.getElementsByTag("li"
                    )) {
                        if (li.text().contains("Thực hành") || li.text().contains("Thực hành")) {
                            System.out.println("vao thuc hanh");
                            Runnable runnable  = new Runnable() {
                                @Override
                                public void run() {
                              synchronized (this){
//                                  String ulTitle = li.getElementsByTag("b").text();
//                                  CongNgheEntity data = new CongNgheEntity();
//                                  data.setUl(ulTitle);
////                                  repositoryDATA.insertCongNghe(data);
//                                  String href = li.getElementsByTag("a").attr("href");
//                                  if (href.contains("..")) {
//                                      href = href.replace("..", "https://vietjack.com");
//                                  }
//                                  System.out.println(href);
//                                  Document document1 = null;
//                                  try {
//                                      document1 = Jsoup.connect(href).get();
//                                  } catch (IOException ex) {
//                                      ex.printStackTrace();
//                                  }
//                                  Element body = document1.getElementsByClass("col-md-7 middle-col").get(0);
//                                  body.getElementsByClass("ads_txt").remove();
//                                  body.getElementsByClass("ads_ads").remove();
//                                  body.getElementsByClass("vj-more").remove();
//                                  body.getElementsByClass("baner-commer").remove();
//                                  System.out.println("data c2" + body);
//                                  boolean delete = true;
//                                  for (Element e :
//                                          body.children()) {
//
//                                      if (delete) {
//                                          if (e.tagName().contains("h2")) {
//                                              delete = false;
//                                          } else {
//                                              e.remove();
//                                          }
//                                      } else {
//                                          if (e.tagName().equals("p") && e.text().length() > 8 && e.text().substring(0, 8).contains("Các bài")) {
//                                              e.remove();
//                                              delete = true;
//                                          }
//                                      }
//                                  }
                                  String ulTitle = li.getElementsByTag("b").text();
                                  String href = li.getElementsByTag("a").attr("href");
                                  if (href.contains("..")) {
                                      href = href.replace("..", "https://vietjack.com");
                                  }
                                  System.out.println(href);
                                  Document document1 = null;
                                  try {
                                      document1 = Jsoup.connect(href).get();
                                  } catch (IOException ex) {
                                      ex.printStackTrace();
                                  }
                                  final Element body = document1.getElementsByClass("col-md-7 middle-col").get(0);
                                  body.getElementsByClass("ads_txt").remove();
                                  body.getElementsByClass("ads_ads").remove();
                                  body.getElementsByClass("vj-more").remove();
                                  body.getElementsByClass("baner-commer").remove();
                                  System.out.println("data c3" + body);
                                  for (int i = 0; i < body.childNodeSize(); i++) {
                                      Node child = body.childNode(i);
                                      if (child.nodeName().equals("#comment")) {
                                          child.remove();
                                      }
                                  }
                                  boolean delete = true;
                                  for (Element e :
                                          body.children()) {
                                      if (delete) {
                                          if (e.tagName().contains("h2")) {
                                              delete = false;
                                          } else {
                                              e.remove();
                                          }
                                      } else {
                                          if (e.tagName().equals("p") && e.text().length() > 10 && e.text().substring(0, 10).contains("Tham khảo") || e.tagName().equals("p") && e.text().length() > 8 && e.text().substring(0, 8).contains("Các bài")) {
                                              e.remove();
                                              delete = true;
                                          }
                                      }
                                  }
                                  for (final Element eimg : body.getElementsByTag("img")) {
                                      Runnable runnable1 = new Runnable() {
                                          @Override
                                          public void run() {
                                              synchronized (this) {
                                                  try {
                                                      String urlImg = eimg.attr("src");
                                                      if (urlImg.contains("..")) {
                                                          urlImg = urlImg.replace("..", "https://vietjack.com");
                                                      }
                                                      String[] splitss = urlImg.split("/");
                                                      String fileName = splitss[splitss.length - 1];
                                                      System.out.println("urlImge  " + urlImg + fileName);
                                                      eimg.getElementsByTag("img").attr("src", "http://abbaserver2.com/app/sachgiai/sgk12/congnghe/" + fileName);
//                                                                        body.getElementsByTag("img").attr("src").replace(urlImg,"http;//blogsv.net"+fileName);
                                                      final DownloadFile downloadFile = new DownloadFile(urlImg, fileName, "congnghe", getApplicationContext());
                                                      if (!urlImg.isEmpty() && urlImg != null) {
                                                          service.execute(new Runnable() {
                                                              @Override
                                                              public void run() {
                                                                  downloadFile.download();
                                                              }
                                                          });

                                                      }
                                                  } catch (Exception e1) {
                                                      System.out.println(e1);
                                                  }
                                              }
                                          }
                                      };
                                      try {
                                          Thread exec = new Thread(runnable1);
                                          exec.start();
                                          exec.join();
                                      } catch (Exception exx) {
                                          exx.printStackTrace();
                                      }

                                  }
                                  String style = "<style>.color-green{color:#008000}</style>";
                                  CongNgheEntity data = new CongNgheEntity();
                                  data.setUl2(ulTitle);
                                  data.setContentUl(style + body.toString());
                                  repositoryDATA.insertCongNghe(data);
                                  Log.d("content finish", "run: " + body.toString());
                                  Runnable runnable = new Runnable() {
                                      @Override
                                      public void run() {
                                          synchronized (this) {
                                              try {

                                              } catch (Exception e) {
                                                  e.printStackTrace();
                                              }

                                              //ssss
                                          }
                                      }
                                  };
//                                                    ss
                                  try {
                                      Thread exec = new Thread(runnable);
                                      exec.start();
                                      exec.join();
                                  } catch (Exception exx) {
                                      exx.printStackTrace();
                                  }
                                  for (final Element e : body.children()
                                  ) {
                                      if (e.tagName().equals("h3") && !e.attr("style").isEmpty()) {
                                          break;
                                      }
                                      if (e.tagName().equals("h2")) {
//                                                CongNgheEntity data = new CongNgheEntity();
//                                                data.setH2(e.text());
//                                                repositoryDATA.insertCongNghe(data);
                                      } else if (e.tagName().equals("h3")) {
                                          CongNgheEntity datan = new CongNgheEntity();
                                          datan.setH3(e.text());
                                          repositoryDATA.insertCongNghe(datan);
                                      } else if (e.tagName().equals("ul")) {
                                          for (final Element li : e.getElementsByTag("li"
                                          )) {
                                              System.out.println("data li" + li);

                                          }
                                      }

                                  }

                              }
                                }
                            };

                            try {
                                Thread thread=  new Thread(runnable);
                                thread.start();
                                thread.join();

                            }catch (Exception e1){
                                e1.printStackTrace();
                            }

                        } else {
                            Runnable runnable = new Runnable() {
                                @Override
                                public void run() {
                                    synchronized (this) {
                                        try {
                                            String ulTitle = li.getElementsByTag("b").text();
                                            CongNgheEntity data = new CongNgheEntity();
                                            data.setUl(ulTitle);
                                            repositoryDATA.insertCongNghe(data);
                                            String href = li.getElementsByTag("a").attr("href");
                                            if (href.contains("..")) {
                                                href = href.replace("..", "https://vietjack.com");
                                            }
                                            System.out.println(href);

                                            Document document1 = Jsoup.connect(href).get();
                                            Element body = document1.getElementsByClass("col-md-7 middle-col").get(0);
                                            body.getElementsByClass("ads_txt").remove();
                                            body.getElementsByClass("ads_ads").remove();
                                            body.getElementsByClass("vj-more").remove();
                                            body.getElementsByClass("baner-commer").remove();
                                            System.out.println("data c2" + body);
                                            boolean delete = true;
                                            for (Element e :
                                                    body.children()) {

                                                if (delete) {
                                                    if (e.tagName().contains("h2")) {
                                                        delete = false;
                                                    } else {
                                                        e.remove();
                                                    }
                                                } else {
                                                    if (e.tagName().equals("p") && e.text().length() > 8 && e.text().substring(0, 8).contains("Các bài")) {
                                                        e.remove();
                                                        delete = true;
                                                    }
                                                }
                                            }
                                            for (final Element e : body.children()
                                            ) {
                                                if (e.tagName().equals("h3") && !e.attr("style").isEmpty()) {
                                                    break;
                                                }
                                                if (e.tagName().equals("h2")) {
//                                                CongNgheEntity data = new CongNgheEntity();
//                                                data.setH2(e.text());
//                                                repositoryDATA.insertCongNghe(data);
                                                } else if (e.tagName().equals("h3")) {
                                                    CongNgheEntity datan = new CongNgheEntity();
                                                    datan.setH3(e.text());
                                                    repositoryDATA.insertCongNghe(datan);
                                                } else if (e.tagName().equals("ul")) {
                                                    for (final Element li : e.getElementsByTag("li"
                                                    )) {
                                                        System.out.println("data li" + li);
                                                        Runnable runnable = new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                synchronized (this) {
                                                                    try {
                                                                        String ulTitle = li.getElementsByTag("b").text();
                                                                        String href = li.getElementsByTag("a").attr("href");
                                                                        if (href.contains("..")) {
                                                                            href = href.replace("..", "https://vietjack.com");
                                                                        }
                                                                        System.out.println(href);
                                                                        Document document1 = Jsoup.connect(href).get();
                                                                        final Element body = document1.getElementsByClass("col-md-7 middle-col").get(0);
                                                                        body.getElementsByClass("ads_txt").remove();
                                                                        body.getElementsByClass("ads_ads").remove();
                                                                        body.getElementsByClass("vj-more").remove();
                                                                        System.out.println("data c3" + body);
                                                                        body.getElementsByClass("baner-commer").remove();
                                                                        for (int i = 0; i < body.childNodeSize(); i++) {
                                                                            Node child = body.childNode(i);
                                                                            if (child.nodeName().equals("#comment")) {
                                                                                child.remove();
                                                                            }
                                                                        }
                                                                        boolean delete = true;
                                                                        for (Element e :
                                                                                body.children()) {

                                                                            if (delete) {
                                                                                if (e.tagName().contains("h2")) {
                                                                                    delete = false;
                                                                                } else {
                                                                                    e.remove();
                                                                                }
                                                                            } else {
                                                                                if (e.tagName().equals("p") && e.text().length() > 10 && e.text().substring(0, 10).contains("Tham khảo") || e.tagName().equals("p") && e.text().length() > 8 && e.text().substring(0, 8).contains("Các bài")) {
                                                                                    e.remove();
                                                                                    delete = true;
                                                                                }
                                                                            }
                                                                        }
                                                                        for (final Element eimg : body.getElementsByTag("img")) {
                                                                            Runnable runnable1 = new Runnable() {
                                                                                @Override
                                                                                public void run() {
                                                                                    synchronized (this) {
                                                                                        try {
                                                                                            String urlImg = eimg.attr("src");
                                                                                            if (urlImg.contains("..")) {
                                                                                                urlImg = urlImg.replace("..", "https://vietjack.com");
                                                                                            }
                                                                                            String[] splitss = urlImg.split("/");
                                                                                            String fileName = splitss[splitss.length - 1];
                                                                                            System.out.println("urlImge  " + urlImg + fileName);
                                                                                            eimg.getElementsByTag("img").attr("src", "http://abbaserver2.com/app/sachgiai/sgk12/congnghe/" + fileName);
//                                                                        body.getElementsByTag("img").attr("src").replace(urlImg,"http;//blogsv.net"+fileName);
                                                                                            final DownloadFile downloadFile = new DownloadFile(urlImg, fileName, "congnghe", getApplicationContext());
                                                                                            if (!urlImg.isEmpty() && urlImg != null) {
                                                                                                service.execute(new Runnable() {
                                                                                                    @Override
                                                                                                    public void run() {
                                                                                                        downloadFile.download();
                                                                                                    }
                                                                                                });

                                                                                            }
                                                                                        } catch (Exception e1) {
                                                                                            System.out.println(e1);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            };
                                                                            try {
                                                                                Thread exec = new Thread(runnable1);
                                                                                exec.start();
                                                                                exec.join();
                                                                            } catch (Exception exx) {
                                                                                exx.printStackTrace();
                                                                            }

                                                                        }


                                                                        String style = "<style>.color-green{color:#008000}</style>";
                                                                        CongNgheEntity data = new CongNgheEntity();
                                                                        data.setUl2(ulTitle);
                                                                        data.setContentUl(style + body.toString());
                                                                        repositoryDATA.insertCongNghe(data);
                                                                        Log.d("content finish", "run: " + body.toString());
                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }

                                                                    //ssss
                                                                }
                                                            }
                                                        };
//                                                    ss
                                                        try {
                                                            Thread exec = new Thread(runnable);
                                                            exec.start();
                                                            exec.join();
                                                        } catch (Exception exx) {
                                                            exx.printStackTrace();
                                                        }
                                                    }
                                                }

                                            }

//                                        CongNgheEntity data = new CongNgheEntity();
//                                        data.setUl(ulTitle);
//                                        repositoryDATA.insertCongNghe(data);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            };
                            try {
                                Thread exec = new Thread(runnable);
                                exec.start();
                                exec.join();
                            } catch (Exception exx) {
                                exx.printStackTrace();
                            }
                        }

                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
