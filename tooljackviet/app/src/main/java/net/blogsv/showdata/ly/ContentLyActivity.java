package net.blogsv.showdata.ly;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import net.blogsv.database.repository.RepositoryDATA;
import net.blogsv.tooljackviet.R;

public class ContentLyActivity extends AppCompatActivity {
    int id;
    String datacontet;
    RepositoryDATA repositoryDATA;
    WebView webView;
    StringBuilder html;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_data);
        repositoryDATA = new RepositoryDATA(getApplication());
        addControls();
        addDatas();

    }

    private void addDatas() {
        html = new StringBuilder();
        html.append("<head>");
        html.append("<meta charset=\"utf-8\">");
        html.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
        html.append("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">");
        html.append("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>");
        html.append("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js\"></script>");
        html.append("</head>");
        html.append("<body >");
        html.append("<div class=\"container\">");
        html.append("<div class=\"row\">");
        html.append("<div class=\"col-md-12\">");
        html.append(datacontet);
        html.append("</div>");
        html.append("</div>");
        html.append("</div>");
        html.append("</body>");
        webView.loadDataWithBaseURL(null, String.valueOf(html), "text/html", "utf-8", null);
    }

    private void addControls() {
        id = getIntent().getIntExtra("id", 0);
        datacontet = getIntent().getStringExtra("datacontet");
        System.out.println("id  " + id);
//        Toast.makeText(this, datacontet+"", Toast.LENGTH_SHORT).show();
        repositoryDATA.getContentLyById(id).observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
//                System.out.println("data contemt "+ s +datacontet);
            }
        });
        webView = findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setAppCacheEnabled(true);
    }
}
