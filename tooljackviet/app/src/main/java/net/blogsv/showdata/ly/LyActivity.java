package net.blogsv.showdata.ly;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.blogsv.adater.AnhAdapter;
import net.blogsv.adater.LyAdapter;
import net.blogsv.database.entity.AnhEntity;
import net.blogsv.database.entity.LyEntity;
import net.blogsv.database.repository.RepositoryDATA;
import net.blogsv.tooljackviet.R;

import java.util.List;

public class LyActivity extends AppCompatActivity {

    RepositoryDATA repositoryDATA;
    RecyclerView recyclerView;
    AnhAdapter lyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        repositoryDATA = new RepositoryDATA(getApplication());
        addControls();
        addEvents();
    }

    private void addEvents() {
        repositoryDATA.getDataAnh().observe(this, new Observer<List<AnhEntity>>() {
            @Override
            public void onChanged(List<AnhEntity> ly11Entities) {
                    lyAdapter.addItems(ly11Entities);
            }
        });
    }

    private void addControls() {
        recyclerView = findViewById(R.id.rcv_category);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        lyAdapter = new AnhAdapter(getApplicationContext(),null);
        recyclerView.setAdapter(lyAdapter);
    }
}
