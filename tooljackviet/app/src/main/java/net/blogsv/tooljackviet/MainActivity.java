package net.blogsv.tooljackviet;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import net.blogsv.craw.AnhActivity;
import net.blogsv.craw.AnhMoiActivity;
import net.blogsv.craw.CongNgheActivity;
import net.blogsv.craw.DiaActivity;
import net.blogsv.craw.GDCDActivity;
import net.blogsv.craw.HoaActivity;
import net.blogsv.craw.HoaNCActivity;
import net.blogsv.craw.LiActivity;
import net.blogsv.craw.LiNCActivity;
import net.blogsv.craw.SinhActivity;
import net.blogsv.craw.SinhNCActivity;
import net.blogsv.craw.SoanVanActivity;
import net.blogsv.craw.SuActivity;
import net.blogsv.craw.TGTPActivity;
import net.blogsv.craw.TinActivity;
import net.blogsv.craw.ToanActivity;
import net.blogsv.craw.ToanNCActivity;
import net.blogsv.craw.ToolHandActivity;
import net.blogsv.craw.VanMauActivity;
import net.blogsv.retrofit2.ApiService;
import net.blogsv.retrofit2.DownloadFile;
import net.blogsv.showdata.ly.LyActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by LilWind NTS on 9/10/2019.
 * ABBA Studio
 * admin@blogsv.net
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void anh(View view) {
        Intent intent = new Intent(this, AnhActivity.class);
        startActivity(intent);
    }

    public void anhMoi(View view) {
        Intent intent = new Intent(this, AnhMoiActivity.class);
        startActivity(intent);
    }

    public void vanMau(View view) {
        Intent intent = new Intent(this, VanMauActivity.class);
        startActivity(intent);
    }

    public void toan(View view) {
        Intent intent = new Intent(this, ToanActivity.class);
        startActivity(intent);
    }

    public void tin(View view) {
        Intent intent = new Intent(this, TinActivity.class);
        startActivity(intent);
    }

    public void tgtp(View view) {
        Intent intent = new Intent(this, TGTPActivity.class);
        startActivity(intent);
    }

    public void su(View view) {
        Intent intent = new Intent(this, SuActivity.class);
        startActivity(intent);
    }

    public void soanVan(View view) {
        Intent intent = new Intent(this, SoanVanActivity.class);
        startActivity(intent);
    }

    public void sinh(View view) {
        Intent intent = new Intent(this, SinhActivity.class);
        startActivity(intent);

    }

    public void ly(View view) {
        Intent intent = new Intent(this, LiActivity.class);
        startActivity(intent);
    }

    public void hoa(View view) {
        Intent intent = new Intent(this, HoaActivity.class);
        startActivity(intent);
    }
    public void btnShowDATA(View view) {
        Intent intent = new Intent(this, LyActivity.class);
        startActivity(intent);
    }
    public void gdcd(View view) {
        Intent intent = new Intent(this, GDCDActivity.class);
        startActivity(intent);
    }

    public void dia(View view) {
        Intent intent = new Intent(this, DiaActivity.class);
        startActivity(intent);
    }

    public void cn(View view) {

        Intent intent = new Intent(this, CongNgheActivity.class);
        startActivity(intent);
    }

    public void sinhnc(View view) {
        Intent intent = new Intent(this, SinhNCActivity.class);
        startActivity(intent);
    }

    public void hoanc(View view) {
        Intent intent = new Intent(this, HoaNCActivity.class);
        startActivity(intent);
    }

    public void lync(View view) {
        Intent intent = new Intent(this, LiNCActivity.class);
        startActivity(intent);
    }

    public void toannc(View view) {
        Intent intent = new Intent(this, ToanNCActivity.class);
        startActivity(intent);
    }

    public void btnToolTay(View view) {
        Intent intent = new Intent(this, ToolHandActivity.class);
        startActivity(intent);
    }
}
